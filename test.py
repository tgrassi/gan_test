import numpy as np
import matplotlib.pyplot as plt
import keras
from keras.layers import Dense, Dropout, Input
from keras.models import Model,Sequential
from tqdm import tqdm
from keras.layers.advanced_activations import LeakyReLU
from keras.optimizers import Adam
import matplotlib.image as mimg
import glob
import os

# covert rgb image to gray
def rgb2gray(rgb):
    return np.dot(rgb[..., :3], [0.2989, 0.5870, 0.1140])

# load data from images folder
def load_data():
    xdata = []
    for fname in glob.glob("imgs/*png"):
        image = rgb2gray(mimg.imread(fname))
        xdata.append(image)

    xdata = np.array(xdata)

    # reduce resolution to fit GPU memory (and faster training)
    xdata = xdata[:, ::3, ::3]

    # shape is number_of_frames, x, y (only gray, so one value)
    d1, d2, d3 = xdata.shape

    # uncomment to plot loaded images
    #for i in range(d1):
        #plt.axis("off")
        #plt.imshow(xdata[i, :, :], interpolation='nearest', cmap='gray')
        #plt.savefig("ref/image_%05d.png" % i)

    # prepare training set unrolling the x, y dimension of the image
    x_train = xdata.reshape(d1, d2 * d3)

    return x_train, d2, d3

# load data
X_train, imx, imy = load_data()

# optimizer
def adam_optimizer():
    return Adam(lr=0.0002, beta_1=0.5)

# generator
def create_generator(imsize):
    generator=Sequential()
    generator.add(Dense(units=256, input_dim=100))
    generator.add(LeakyReLU(0.2))

    generator.add(Dense(units=256))
    generator.add(LeakyReLU(0.2))

    generator.add(Dense(units=512))
    generator.add(LeakyReLU(0.2))

    generator.add(Dense(units=1024))
    generator.add(LeakyReLU(0.2))
    generator.add(Dense(units=imsize, activation='tanh'))

    generator.compile(loss='binary_crossentropy', optimizer=adam_optimizer())
    return generator

# discriminator
def create_discriminator(imsize):
    discriminator=Sequential()
    discriminator.add(Dense(units=1024, input_dim=imsize))
    discriminator.add(LeakyReLU(0.2))
    discriminator.add(Dropout(0.3))

    discriminator.add(Dense(units=512))
    discriminator.add(LeakyReLU(0.2))
    discriminator.add(Dropout(0.3))

    discriminator.add(Dense(units=256))
    discriminator.add(LeakyReLU(0.2))
    discriminator.add(Dropout(0.3))

    discriminator.add(Dense(units=256))
    discriminator.add(LeakyReLU(0.2))

    discriminator.add(Dense(units=1, activation='sigmoid'))

    discriminator.compile(loss='binary_crossentropy', optimizer=adam_optimizer())
    return discriminator

# put generator and discriminator together into the model
def create_gan(discriminator, generator):
    discriminator.trainable = False
    gan_input = Input(shape=(100,))
    x = generator(gan_input)
    gan_output = discriminator(x)
    gan = Model(inputs=gan_input, outputs=gan_output)
    gan.compile(loss='binary_crossentropy', optimizer='adam')
    return gan

# generate new images and save them to file
def plot_generated_images(epoch, generator, imx, imy, examples=100):

    # create folder if not exists
    generated_folder = 'generated'
    if not os.path.exists(generated_folder):
        os.makedirs(generated_folder)

    # noise to generate images
    noise = np.random.normal(loc=0, scale=1, size=[examples, 100])
    generated_images = generator.predict(noise)
    generated_images = generated_images.reshape(100, imx, imy)
    for i in range(generated_images.shape[0]):
        plt.clf()
        plt.imshow(generated_images[i], interpolation='nearest', cmap='gray')
        plt.axis('off')
        # plt.savefig('generated/image_%05d_%05d.png' % (epoch, i))
        plt.savefig(generated_folder + '/image_%05d.png' % i)


# do training for a given number of epochs
def training(imx, imy, epochs=1, batch_size=128, e_generate=20):

    # Loading the data
    X_train, lmx, lmy = load_data()
    batch_count = X_train.shape[0] / batch_size

    # Creating GAN
    generator = create_generator(imx * imy)
    discriminator = create_discriminator(imx * imy)
    gan = create_gan(discriminator, generator)

    # loop on epochs
    for e in range(0, epochs):
        print("Epoch %d" %e)
        for _ in tqdm(range(batch_size)):
            #generate random noise as an input to initialize the generator
            noise = np.random.normal(0, 1, [batch_size, 100])

            # Generate fake images from noised input
            generated_images = generator.predict(noise)

            # Get a random set of real images
            image_batch = X_train[np.random.randint(low=0, high=X_train.shape[0], size=batch_size)]

            # Construct different batches of real and fake data
            X = np.concatenate([image_batch, generated_images])

            # Labels for generated and real data
            y_dis = np.zeros(2*batch_size)
            y_dis[:batch_size] = 0.9

            # Pre train discriminator on fake and real data before starting the gan.
            discriminator.trainable = True
            discriminator.train_on_batch(X, y_dis)

            # Tricking the noised input of the Generator as real data
            noise = np.random.normal(0, 1, [batch_size, 100])
            y_gen = np.ones(batch_size)

            # During the training of gan,
            # the weights of discriminator should be fixed.
            # We can enforce that by setting the trainable flag
            discriminator.trainable = False

            # training the GAN by alternating the training of the Discriminator
            # and training the chained GAN model with Discriminator's weights freezed.
            gan.train_on_batch(noise, y_gen)

        # generate images every e_generate epochs
        if e % e_generate == 0:
            plot_generated_images(e, generator, imx, imy)

# do training for
training(imx, imy, epochs=1000)

# say goobye
print("done!")


