# GAN test #
Testing GAN with KERAS+TF on GPUs to generate movie frames follwing this blog post [https://medium.com/datadriveninvestor/generative-adversarial-network-gan-using-keras-ce1c05cfdfd3].    
The traing set comes from a subset of frames from the ending sequence of Fellini's "8 e 1/2" [https://www.youtube.com/watch?v=m-V0uuIteJE].     
HW: GeForce GTX 1050 Ti, 4Gb       

Results after few hundreds epochs are like:

![picture](https://bitbucket.org/tgrassi/gan_test/raw/c7d94c9c8e5d86d7b1e7051a1bc6bb25f362423f/sample/image_00062.png)      
![picture](https://bitbucket.org/tgrassi/gan_test/raw/72b8670f931297533e1388744073896cf839e547/sample/image_00093.png)       
![picture](https://bitbucket.org/tgrassi/gan_test/raw/72b8670f931297533e1388744073896cf839e547/sample/image_00044.png)       
![picture](https://bitbucket.org/tgrassi/gan_test/raw/72b8670f931297533e1388744073896cf839e547/sample/image_00039.png)       



